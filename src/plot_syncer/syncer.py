import logging
import os
import subprocess
import sys
import time
from datetime import datetime

from typing import List

from plot_syncer.window import Window

VERBOSE = False


def is_tool(name):
    """Check whether `name` is on PATH and marked as executable."""
    from shutil import which

    return which(name) is not None


def resolve_window(windows: List[Window], current_hour=None) -> Window:
    if current_hour is None:
        current_hour = datetime.today().hour

    for window in windows:
        from_hour = window.from_hour
        to_hour = window.to_hour

        wrap_around = from_hour > to_hour
        if (not wrap_around and (from_hour <= current_hour < to_hour)) or (
            wrap_around and (from_hour <= current_hour or current_hour < to_hour)
        ):
            return window

    return None


def synchronize(source, destination, windows: List[Window], verbose=False):
    global VERBOSE
    VERBOSE = verbose

    os.makedirs(destination, exist_ok=True)
    if not is_tool("rsync"):
        print("rsync is not installed")
        sys.exit(2)
    args = [
        "rsync",
        "-az",
        "--delete",
        "--append",
        "--time-limit",
        "60",
        "-e",
        "ssh -o StrictHostKeyChecking=no",
        source,
        destination,
    ]
    if VERBOSE:
        args.append("-v")

    while True:
        current_hour = datetime.today().hour
        window = resolve_window(windows, current_hour)
        if window:
            logging.info(f"Resolved current window {window} ")

            extra_args = []
            if window.rate_limit > 0:
                extra_args = [*extra_args, "--bwlimit", f"{window.rate_limit/8}m"]
            subprocess.run([*args, *extra_args])
        else:
            logging.info(f"No time window found for hour {current_hour}")
        time.sleep(60)
