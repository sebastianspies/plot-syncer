#!/usr/bin/env sh

cp -vr /root/.ssh-import /root/.ssh
chown root:root /root/.ssh -R

python main.py $@
