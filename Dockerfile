FROM python:3.8-slim

RUN apt-get update; apt-get install -y rsync ssh; apt-get clean

WORKDIR /app

COPY ./requirements.txt /app

RUN pip install -r requirements.txt

COPY ./src /app
COPY ./init.sh /app
COPY ./main.py /app

COPY ./tests /app
COPY ./conftest.py /app
RUN pytest

ENTRYPOINT [ "/app/init.sh" ]

